﻿using Business.DataTransferObjects;
using Data.DataContext;
using Data.RepositoryContract;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.RepositoryImplementation
{
    public class PessoaRepository : BaseRepository, IPessoaRepository
    {

        public PessoaRepository(Context context) : base(context)
        {

        }

        public void AdicionarValoresMocados(PessoaDTO dto)
        {
            if (_context.Pessoas.Count() == 0)
            {
                _context.Pessoas.Add(dto);
                _context.SaveChanges();
            }
        }

        public async Task<IEnumerable<PessoaDTO>> GetPessoas()
        {
            return await _context.Pessoas.ToListAsync();
        }

        public async Task<PessoaDTO> GetPessoaById(long id)
        {
            return await _context.Pessoas.FindAsync(id);
        }

        public async void UpdatePessoa(long id, PessoaDTO pessoa)
        {
            _context.Entry(pessoa).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public async void CreatePessoa(PessoaDTO pessoa)
        {
            _context.Pessoas.Add(pessoa);
            await _context.SaveChangesAsync();
        }

        public async void DeletePessoa(PessoaDTO pessoa)
        {
            _context.Pessoas.Remove(pessoa);
            await _context.SaveChangesAsync();
        }

        public bool PessoaExists(long id)
        {
            return _context.Pessoas.Any(e => e.Id == id);
        }


    }
}
