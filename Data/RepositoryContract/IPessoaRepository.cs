﻿using Business.DataTransferObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.RepositoryContract
{
    public interface IPessoaRepository
    {
        void AdicionarValoresMocados(PessoaDTO dto);
        Task<IEnumerable<PessoaDTO>> GetPessoas();
        Task<PessoaDTO> GetPessoaById(long id);
        bool PessoaExists(long id);
        void CreatePessoa(PessoaDTO pessoa);
        void DeletePessoa(PessoaDTO pessoa);
        void UpdatePessoa(long id, PessoaDTO pessoa);
    }
}
