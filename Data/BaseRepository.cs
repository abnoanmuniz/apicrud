﻿using Data.DataContext;

namespace Data
{
    public class BaseRepository
    {
        public readonly Context _context;

        public BaseRepository(Context context)
        {
            _context = context;
        }
    }
}
