﻿using System;

namespace Business.DataTransferObjects
{
    public class PessoaDTO
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Email { get; set; }
        public int Genero { get; set; }
        public string Endereco { get; set; }
    }
}
