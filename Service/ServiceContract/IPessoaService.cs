﻿using Business.DataTransferObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.ServiceContract
{
    public interface IPessoaService
    {
        void AdicionarValoresMocados();
        Task<IEnumerable<PessoaDTO>> GetPessoas();
        Task<PessoaDTO> GetPessoaById(long id);
        void CreatePessoa(PessoaDTO pessoa);
        void DeletePessoa(PessoaDTO pessoa);
        void UpdatePessoa(long id, PessoaDTO pessoa);
    }
}
