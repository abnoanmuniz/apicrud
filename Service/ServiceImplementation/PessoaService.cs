﻿using Business.DataTransferObjects;
using Data.RepositoryContract;
using Service.ServiceContract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.ServiceImplementation
{
    public class PessoaService : IPessoaService
    {
        private readonly IPessoaRepository _pessoaRepository;

        public PessoaService(IPessoaRepository pessoaRepository)
        {
            _pessoaRepository = pessoaRepository;
        }
        public void AdicionarValoresMocados()
        {
            var dataNasc = DateTime.ParseExact("21/05/1991", "d", null);
            PessoaDTO pessoa = (new PessoaDTO
            {
                Nome = "Abnoan Muniz",
                Cpf = "09088606518",
                DataNascimento = dataNasc,
                Email = "abnoanmuniz@gmail.com",
                Endereco = "Estrada de belem, 175",
                Genero = 0
            });
            _pessoaRepository.AdicionarValoresMocados(pessoa);
        }

        public Task<IEnumerable<PessoaDTO>> GetPessoas()
        {
            return _pessoaRepository.GetPessoas();
        }

        public Task<PessoaDTO> GetPessoaById(long id)
        {
            return _pessoaRepository.GetPessoaById(id);
        }

        public void UpdatePessoa(long id, PessoaDTO pessoa)
        {
            _pessoaRepository.UpdatePessoa(id, pessoa);
        }

        public void CreatePessoa(PessoaDTO pessoa)
        {
            _pessoaRepository.CreatePessoa(pessoa);
        }

        public void DeletePessoa(PessoaDTO pessoa)
        {
            _pessoaRepository.DeletePessoa(pessoa);
        }

        #region Private Methods

        private bool PessoaExists(long id)
        {
            return _pessoaRepository.PessoaExists(id);
        }
        #endregion
    }
}
