﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Util
{
          public class GenericRepository<TObject> : IRepository<TObject> where TObject : class
        {
            protected GenericDbContext _context;
            protected DbSet<TObject> _set;

            public GenericRepository(GenericDbContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException("context");
                }

                _context = context;
                _set = context.Set<TObject>();
            }

            /// <summary>
            /// DbContext usado nesse repositório.
            /// </summary>
            protected GenericDbContext Context
            {
                get
                {
                    return _context;
                }
                set
                {
                    this._context = value;
                }
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposing)
                {
                    if (Context != null)
                    {
                        Context.Dispose();
                        if (GenericsDbContextAplication.GenericsbContexts != null && GenericsDbContextAplication.GenericsbContexts.Count > 0)
                        {
                            GenericsDbContextAplication.ClearGenericsbContexts();
                        }

                        // Context = null;
                    }
                }
            }

            protected IOrderedQueryable<TObject> GenerateOrderBySelector<TKey>(IQueryable<TObject> query, string sortOrder, Expression<Func<TObject, TKey>> keySelector)
            {
                return sortOrder != null && sortOrder.ToUpper() == "DESC" ? query.OrderByDescending(keySelector) : query.OrderBy(keySelector);
            }

            /// <summary>
            /// Realiza uma busca paginada, usando uma restrição de busca (where).
            /// </summary>
            /// <param name="criteria">Critério de busca. (where)</param>
            /// <param name="pagina">Página a ser consultada (iniciando em 1). Se não for passado nenhum valor (null) assume a pagina 1</param>
            /// <param name="qtdResultsInPage">Quantidade de resultados por página. Se não for passado nenhum valor (null) assume o valor padrão em configuração.</param>
            /// <param name="ordenacao">Função usada na ordenação. Baseada nas propriedades da entidade.</param>
            /// <returns>Resultados na página atual</returns>
            protected PagedResult<TObject> FindPaged(Func<IQueryable<TObject>, IOrderedQueryable<TObject>> ordenacao, Expression<Func<TObject, bool>> criteria = null, int? pagina = null, int? qtdResultsInPage = null)
            {
                int resultsByPage = qtdResultsInPage ?? 10;

                int paginaAtual = pagina ?? 1;
                int pular = pagina.HasValue ? (pagina.Value - 1) * resultsByPage : 0;

                var criteriaConsulta = criteria ?? (e => true);

                IEnumerable<TObject> lista;
                lista = ordenacao(Context.Set<TObject>().Where(criteriaConsulta))
                        .Skip(pular)
                        .Take(resultsByPage);


                int quantidadeTotal = Context.Set<TObject>().Where(criteriaConsulta).Count();

                return new PagedResult<TObject>()
                {
                    List = lista.ToList(),
                    TotalCount = quantidadeTotal,
                    CurrentPage = paginaAtual,
                    PageSize = resultsByPage
                };

            }

            protected PagedResult<TObject> FindPagedLinqkit(Func<IQueryable<TObject>, IOrderedQueryable<TObject>> ordenacao, Expression<Func<TObject, bool>> criteria = null, int start = 0, int qtdResultsInPage = 10, params string[] includes)
            {
                var criteriaConsulta = criteria ?? (e => true);
                var query = Context.Set<TObject>().AsQueryable();

                if (includes != null && includes.Any())
                {
                    foreach (var include in includes) { query = query.Include(include); }
                }

                query = query.AsExpandable().Where(criteriaConsulta);

                int quantidadeTotal = query.Count();
                var lista = ordenacao(query).Skip(start).Take(qtdResultsInPage).ToList();

                return new PagedResult<TObject>()
                {
                    List = lista,
                    TotalCount = quantidadeTotal,
                    CurrentPage = start,
                    PageSize = qtdResultsInPage
                };

            }

            public ICollection<TObject> GetAll()
            {
                return Context.Set<TObject>().ToList();
            }

            public int GetCountByQuery(Expression<Func<TObject, bool>> exp)
            {
                return Context.Set<TObject>().Where(exp).Count();
            }

            public async Task<ICollection<TObject>> GetAllAsync()
            {
                return await Context.Set<TObject>().ToListAsync();
            }

            public TObject GetById(long id)
            {
                return Context.Set<TObject>().Find(id);
            }

            public TObject GetById<T>(T id)
            {
                return Context.Set<TObject>().Find(id);
            }

            public async Task<TObject> GetAsync(long id)
            {
                return await Context.Set<TObject>().FindAsync(id);
            }

            public async Task<TObject> FindAsync(Expression<Func<TObject, bool>> match)
            {
                return await Context.Set<TObject>().SingleOrDefaultAsync(match);
            }

            public ICollection<TObject> FindAll(Expression<Func<TObject, bool>> match)
            {
                return Context.Set<TObject>().Where(match).ToList();
            }

            public async Task<ICollection<TObject>> FindAllAsync(Expression<Func<TObject, bool>> match)
            {
                return await Context.Set<TObject>().Where(match).ToListAsync();
            }

            public TObject Add(TObject t, bool saveChanges = false)
            {
                Context.Set<TObject>().Add(t);
                if (saveChanges)
                {
                    this.Context.SaveChanges();
                }
                return t;
            }

            public async Task<TObject> AddAsync(TObject t)
            {
                Context.Set<TObject>().Add(t);
                return t;
            }

            public ICollection<TObject> AddRange(ICollection<TObject> collection)
            {
                Context.Set<TObject>().AddRange(collection);
                return collection;
            }

            public ICollection<TObject> BulkInsert(ICollection<TObject> collection, Action<string> logger = null)
            {
                if (logger != null)
                {
                    Context.BulkInsert(collection, operation => operation.Log += logger);
                }
                else
                {
                    Context.BulkInsert(collection);
                }
                return collection;
            }

            public TObject Update(TObject updated, long key)
            {
                if (updated == null)
                    return null;

                TObject existing = Context.Set<TObject>().Find(key);
                if (existing != null)
                {
                    Context.Entry(existing).CurrentValues.SetValues(updated);

                }
                return existing;
            }

            public ICollection<TObject> BulkUpdate(ICollection<TObject> collection, Action<string> logger = null)
            {
                if (logger != null)
                {
                    Context.BulkUpdate(collection, operation => operation.Log += logger);
                }
                else
                {
                    Context.BulkUpdate(collection);
                }
                return collection;
            }

            public TObject Update(TObject updated)
            {
                if (updated == null)
                    return null;

                TObject existing = Context.Set<TObject>().Find(updated.GetType().GetProperty("Id").GetValue(updated));
                if (existing != null)
                {
                    Context.Entry(existing).CurrentValues.SetValues(updated);
                }

                return existing;
            }

            public TObject Update(TObject updated, bool saveChanges)
            {
                TObject existing = Context.Set<TObject>().Find(updated.GetType().GetProperty("Id").GetValue(updated));
                if (saveChanges && existing != null)
                {
                    Context.Entry(existing).CurrentValues.SetValues(updated);
                    Context.SaveChanges();
                }

                return existing;
            }

            public async Task<TObject> UpdateAsync(TObject updated, long key)
            {
                if (updated == null)
                    return null;

                TObject existing = await Context.Set<TObject>().FindAsync(key);
                if (existing != null)
                {
                    Context.Entry(existing).CurrentValues.SetValues(updated);
                }
                return existing;
            }

            public void Delete(TObject t)
            {
                var key = t.GetType().GetProperty("Id").GetValue(t);
                Context.Set<TObject>().Remove(Context.Set<TObject>().Find(key));
            }

            public void BulkDelete(ICollection<TObject> collection, Action<string> logger = null)
            {
                if (logger != null)
                {
                    Context.BulkDelete(collection, operation => operation.Log += logger);
                }
                else
                {
                    Context.BulkDelete(collection);
                }
            }

            public void Delete(int id)
            {
                TObject entity = Context.Set<TObject>().Find(id);
                Context.Set<TObject>().Remove(entity);
            }

            public async Task<int> DeleteAsync(TObject t)
            {
                Context.Set<TObject>().Remove(t);
                return await Context.SaveChangesAsync();
            }

            public int Count()
            {
                return Context.Set<TObject>().Count();
            }

            public async Task<int> CountAsync()
            {
                return await Context.Set<TObject>().CountAsync();
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            public bool Any(Expression<Func<TObject, bool>> match)
            {
                return Context.Set<TObject>().Any(match);
            }

            public virtual PagedResult<TObject> Find(BaseFilter<TObject> filters)
            {
                if (filters == null)
                {
                    throw new ArgumentNullException(nameof(filters));
                }

                int quantidadeRegistros = filters.Where == null ? Count() : Count(filters.Where);
                if (quantidadeRegistros < filters.PageSize && filters.Page > 0)
                {
                    filters.Page = 0;
                }

                IList<TObject> dados = GetData(filters);
                return new PagedResult<TObject>()
                {
                    CurrentPage = quantidadeRegistros > filters.PageSize ? filters.Page : 0,
                    PageSize = filters.PageSize,
                    List = dados,
                    TotalCount = quantidadeRegistros,
                    TotalPages = filters.PageSize > 0 ? (int)Math.Ceiling((decimal)quantidadeRegistros / filters.PageSize) : 0
                };
            }

            /// <summary>
            /// Obtém a quantidade de registros filtrados
            /// </summary>
            /// <param name="where">Filtros</param>
            /// <returns>Quantidade de registros filtrados</returns>
            public virtual int Count(Expression<Func<TObject, bool>> where)
            {
                if (where == null)
                {
                    throw new ArgumentNullException("where");
                }

                return _set.Count(where);
            }

            public void RemoveRange(ICollection<TObject> collection)
            {
                foreach (var item in collection.ToList())
                {
                    _set.Remove(item);
                }

                this.Context.SaveChanges();
            }

            #region Private Methods

            /// <summary>
            /// Obtém os registros da entidade filtrados
            /// </summary>
            /// <param name="filters">Filtros</param>
            /// <returns>Coleção da entidade</returns>
            private IList<TObject> GetData(BaseFilter<TObject> filters)
            {
                if (filters == null)
                {
                    throw new ArgumentNullException(nameof(filters));
                }

                IQueryable<TObject> result = _set;
                if (filters.Where != null)
                {
                    result = result.Where(filters.Where);
                }

                foreach (var include in filters.Includes)
                {
                    result = QueryableExtensions.Include(result, (dynamic)include);
                }

                return SortQuery(result, filters)
                       .Skip(filters.Page * filters.PageSize)
                       .Take(filters.PageSize)
                       .ToList();
            }

            private static IQueryable<TObject> SortQuery(IQueryable<TObject> list, BaseFilter<TObject> filters)
            {
                string sortDirection = string.IsNullOrEmpty(filters.OrderType) ? "asc" : "desc";

                if (!string.IsNullOrEmpty(filters.OrderColumnName))
                {
                    list = list.OrderBy(filters.OrderColumnName, sortDirection == "desc");
                }
                else if (filters.SortExpression != null)
                {
                    if (sortDirection.Contains("asc"))
                    {
                        list = list.OrderBy(filters.SortExpression);
                    }
                    else
                    {
                        list = list.OrderByDescending(filters.SortExpression);
                    }
                }
                else
                {
                    list = list.OrderBy("Id", sortDirection == "desc");
                }

                return list;
            }

            #endregion

        }
    }

