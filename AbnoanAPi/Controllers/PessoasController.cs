﻿using AbnoanAPi.Models;
using AutoMapper;
using Business.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using Service.ServiceContract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AbnoanAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PessoasController : ControllerBase
    {
        private readonly IPessoaService _pessoaService;
        private readonly IMapper _mapper;



        public PessoasController(IPessoaService pessoaService, IMapper mapper)
        {
            _pessoaService = pessoaService;
            _mapper = mapper;
            _pessoaService.AdicionarValoresMocados();
        }

        // GET: api/Pessoas
        [HttpGet]
        [ProducesResponseType(typeof(IList<Pessoa>), 200)]
        public async Task<ActionResult<IEnumerable<Pessoa>>> GetPessoas()
        {
            return Ok(await _pessoaService.GetPessoas().ConfigureAwait(false));
        }

        // GET: api/Pessoas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Pessoa>> GetPessoa(long id)
        {
            var pessoa = _pessoaService.GetPessoaById(id);

            if (pessoa == null)
            {
                return NotFound();
            }

            return Ok(await pessoa);
        }

        //PUT: api/Pessoas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPessoa(long id, Pessoa pessoa)
        {
            if (id != pessoa.Id)
            {
                return BadRequest();
            }
            await Task.Run(() => _pessoaService.UpdatePessoa(id, _mapper.Map<PessoaDTO>(pessoa)));
            return Ok(pessoa);            
        }

        // POST: api/Pessoas
        [HttpPost]
        public async Task<ActionResult<Pessoa>> PostPessoa(Pessoa pessoa)
        {
            await Task.Run(() => _pessoaService.CreatePessoa(_mapper.Map<PessoaDTO>(pessoa)));
            return CreatedAtAction("GetPessoa", new { id = pessoa.Id }, pessoa);
        }

        // DELETE: api/Pessoas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Pessoa>> DeletePessoa(long id)
        {
            var pessoa = await _pessoaService.GetPessoaById(id);
            if (pessoa == null)
            {
                return NotFound();
            }
            _pessoaService.DeletePessoa(_mapper.Map<PessoaDTO>(pessoa));
            return NoContent();
        }

    }
}
