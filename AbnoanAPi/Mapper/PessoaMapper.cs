﻿using AbnoanAPi.Models;
using AutoMapper;
using Business.DataTransferObjects;

namespace AbnoanAPi.Mapper
{
    public class PessoaMapper : Profile
    {
        public PessoaMapper()
        {
            CreateMap<Pessoa, PessoaDTO>()
                .ForMember(pe => pe.DataNascimento, opt => opt.MapFrom(src => src.DataNascimento.ToString("dd/MM/yyyy")))
                .ReverseMap();                
        }      
    }
}
