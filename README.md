REST API exemplos de chamada


Requisitando uma lista de pessoas

Request

GET /api/pessoas

`http://localhost:35322/api/pessoas`

Response

```
HTTP/1.1 200 OK
Date: Thu, 29 Aug 2019 17:09:21 GMT
Status: 200 OK
Connection: close
Content-Type: application/json
Content-Length: 250

[
    {
        "id": 1,
        "nome": "Abnoan Muniz",
        "cpf": "09088606518",
        "dataNascimento": "1991-05-21T00:00:00",
        "email": "abnoanmuniz@gmail.com",
        "genero": 0,
        "endereco": "Estrada de belem, 175"
    }
]
```



Criando uma nova Pessoa

Request

POST /api/pessoas

`http://localhost:35322/api/pessoas`

```
Response

HTTP/1.1 201 Created
Date: Thu, 29 Aug 2019 17:14:42 GMT
Status: 201 Created
Connection: close
Content-Type: application/json
Location: http://localhost:35322/api/Pessoas/0
Content-Length: 211

{
    "id": 0,
    "nome": "Juliana Pereira",
    "cpf": "09088606519",
    "dataNascimento": "1991-05-21T00:00:00",
    "email": "jupereira@gmail.com",
    "genero": 0,
    "endereco": "Estrada de belem, 176"
}
```


Recuperando uma pessoa existente

Request

GET /api/pessoas/id

`http://localhost:35322/api/pessoas/1`

Response

```
HTTP/1.1 200 OK
Date: Thu, 29 Aug 2019 17:19:35 GMT
Status: 200 OK
Connection: close
Content-Type: application/json
Content-Length: 210

{
    "id": 1,
    "nome": "Abnoan Muniz",
    "cpf": "09088606518",
    "dataNascimento": "1991-05-21T00:00:00",
    "email": "abnoanmuniz@gmail.com",
    "genero": 0,
    "endereco": "Estrada de belem, 175"
}
```



Atualizando uma pessoa

Request

PUT /api/pessoas/id

`http://localhost:35322/api/pessoas/1`

Response

```
HTTP/1.1 200 OK
Date: Thu, 29 Aug 2019 17:35:22 GMT
Status: 200 OK
Connection: close
Content-Type: application/json
Content-Length: 219

{
    "id": 1,
    "nome": "Abnoan Muniz e Silva",
    "cpf": "09088606518",
    "dataNascimento": "1991-05-21T00:00:00",
    "email": "abnoanmuniz@gmail.com",
    "genero": 0,
    "endereco": "av dos guararapes, 924"
}
```


Deletando uma pessoa

Request

DELETE /api/pessoas/id

`http://localhost:35322/api/pessoas/2`

Response

```
HTTP/1.1 204 No Content
Date: Thu, 29 Aug 2019 17:32:26 GMT
Status: 204 No Content
Connection: close
```


Deletando a mesma pessoa outra vez

Request

DELETE /api/pessoas/id

`http://localhost:35322/api/pessoas/2`

Response

```
HTTP/1.1 404 Not Found
Date: Thu, 29 Aug 2019 17:39:44 GMT
Status: 404 Not Found
Connection: close
Content-Type: application/json
Content-Length: 167

{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Not Found",
    "status": 404,
    "traceId": "80000005-0001-fd00-b63f-84710c7967bb"
}
```


